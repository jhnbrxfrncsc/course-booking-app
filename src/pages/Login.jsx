import React, { useState, useEffect, useContext } from 'react';

// user context
import UserContext from '../UserContext';

// react-bootstrap components
import { 
    Button, 
    Col, 
    Container, 
    Form, 
    Row 
} from 'react-bootstrap';

// react-router
import { useHistory, Redirect } from 'react-router-dom';

// sweetalert
import Swal from "sweetalert2";


const Login = () => {
    // useContext is used to unpack the data from the UserContext
    const { user, setUser } = useContext(UserContext);
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });
    const [isDisabled, setIsDisabled] = useState(true);

    // history
    const history = useHistory();

    // localstorage
    const token = localStorage.getItem("token");

    // Form Data destructuring
    const { email, password } = formData;

    // Use Effect
    useEffect(() => {
        if( email !== "" && password !== "" ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password])

    // events handler
    const handleChange = (e) => setFormData({...formData, [e.target.name]: e.target.value});
    
    const handleSubmit = (e) => {
        e.preventDefault();
        setUser({
            email: localStorage.getItem("email")
        });
        fetch("https://sheltered-reaches-76330.herokuapp.com/api/users/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: formData.email,
                password: formData.password
            })
        })
            .then(res => res.json())
            .then(res => {
                if(res.token){
                    // sweetalert
                    Swal.fire({
                        title: "Login Successful", 
                        icon: "success",
                        text: res.message
                    });

                    // storing state to local storage
                    localStorage.setItem("token", res.token);
                    localStorage.setItem("email", res.result.email);
                    localStorage.setItem("firstName", res.result.firstName);
                    localStorage.setItem("lastName", res.result.lastName);
                    localStorage.setItem("isAdmin", res.result.isAdmin);

                    // State reset
                    setFormData({
                        email: "",
                        password: ""
                    });

                    setUser({
                        id: res.result._id,
                        isAdmin: res.result.isAdmin
                    })
                    
                    // Redirecting to course page
                    history.push('/courses');
                } else {
                    // error alert message
                    Swal.fire({
                        title: res.message, 
                        icon: "error",
                        text: "Check you login credentials and try again."
                    });
                }
        });
    }

    return (
        <>
            {
                !token ? (
                    <Container className="my-5">
                        <h1 className="text-center">Login</h1>
                        <Row className="justify-content-center mt-4">
                            <Col xs={10} md={6}>
                                <Form
                                    className="border p-3"
                                    onSubmit={handleSubmit}
                                >

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="email"
                                    >
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.email}
                                            name="email"
                                            type="email" 
                                            placeholder="Enter email" 
                                        />
                                    </Form.Group>

                                    <Form.Group 
                                        className="mb-3" 
                                        controlId="password"
                                    >
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control 
                                            onChange={handleChange}
                                            value={formData.password}
                                            name="password"
                                            type="password"
                                            placeholder="Password"
                                        />
                                    </Form.Group>

                                    <Button 
                                        variant="primary" 
                                        type="submit"
                                        className="btn-info"
                                        disabled={isDisabled}
                                    >
                                        Submit
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                ) : (
                    <Redirect to="/courses" />
                )
            }
        </>
    )
}

export default Login
