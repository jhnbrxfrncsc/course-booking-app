import React, { useContext } from 'react';

// user context
import UserContext from '../../UserContext';

// react-router
import { Link } from 'react-router-dom';

// react-bootstrap components
import {
    Navbar,
    Nav
} from 'react-bootstrap';


const Header = () => {
    const { user } = useContext(UserContext);
    const token = localStorage.getItem("token");
    // console.log(user);
    // const email = localStorage.getItem("email");
    // const [user, setUser] = useState(email);

    // useEffect(() => {
    //     if(email){
    //         setUser(email)
    //     } else {
    //         setUser(null)
    //     }
    // }, [email]);

    return (
        <Navbar bg="info" expand="lg">
            <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
                    {
                        token ? (
                            <> 
                                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                            </>
                        ) : (
                            <>
                                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            </>
                        )
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;
