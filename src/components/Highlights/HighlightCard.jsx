import React from 'react';
import { 
    Button,
    Card 
} from 'react-bootstrap';

const HighlightCard = ({photo, title, desc}) => {
    return (
        <Card>
            <Card.Img variant="top" src={photo} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{desc}</Card.Text>
                <Button variant="info">Enroll</Button>
            </Card.Body>
        </Card>
    )
}

export default HighlightCard;
