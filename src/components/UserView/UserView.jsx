import React, { useEffect, useState } from 'react';

// react-bootstrap
import { Col } from 'react-bootstrap';

// router
import { useParams } from 'react-router-dom';

// components
import CourseCard from '../CoursesCard/CourseCard';

const UserView = ({courses}) => {
    const [course, setCourse] = useState();
    const { courseId } = useParams()

    useEffect(() => {
        // Fetch data
        fetch(`https://sheltered-reaches-76330.herokuapp.com/api/courses/${courseId}`)
            .then(res => res.json())
            .then(res => {
                console.log(res);
            })
    }, [])
    
    return (
        <>
            {
                courses.filter(course => course.isActive).map(course => {
                    return (
                        <Col 
                            key={course._id}
                            sm={3}
                            md={4}
                        >
                            <CourseCard 
                                _id = {course._id}
                                title = {course.courseName}
                                desc = {course.courseDesc}
                                price = {course.price}
                            />
                        </Col>
                    )
                })
            } 
        </>
    )
}

export default UserView;
