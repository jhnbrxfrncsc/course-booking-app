import React from 'react';

// react-bootstrap components
import {
    Button
} from 'react-bootstrap';

// react-router
import { Link } from 'react-router-dom';

const Banner = ({title, content, dest, btnLabel}) => {
    return (
        <div className="jumbotron">
            <h1>{title}</h1>
            <p>{content}</p>
            <Button as={Link} to={dest} variant="info">{btnLabel}</Button>
        </div>
    )
}

export default Banner;